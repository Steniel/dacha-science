# real estate price estimation #

### info ###

this repo contains jupyter notebook with several models of price prediction.

* all source data is saved in html pages of avito output
* the script parses source code to extract all important data
* few models for price formation are used
* each one is validated with test data set
* the model was used to estimate my own house and the predicted price was succesfully used for sale. 